Electron vous permettra de créer des applications parfaitement intégrées à Windows, Linux ou Mac OS X simplement avec du HTML, du CSS et un peu de JavaScript.

De nombreuses applications comme Atom, Visual Studio Code, ou encore le navigateur Brave sont basées sur Electron et permettent de créer rapidement de belles interfaces parfaitement intégrées au système d’exploitation, quel qu’il soit.

Documentation d'Electron : https://electronjs.org/docs

**Prérequis nécessaires**    
 Connaître un minimum HTML5, CSS3 et JavaScript.    
 Ne pas avoir peur d’utiliser la ligne de commande et savoir l’utiliser basiquement (ouverture, `cd` …).    
 Disposer de Node.js, ou au moins de npm.

 **Découvrir**    
 Les APIs de Node.js.    
 JavaScript dans sa version ES6.

**Objectifs**    
 Réalisation d'une application avec Electrobn et ses API spécifiques.
 Savoir utiliser `electron-packager` pour distribuer ces applications.

**Installation**   
Pour cela vous devez avoir installer NPM puis ouvrir un terminal en mode *Administrateur* ou en *root* (selon les systèmes), puis exécutez ceci.

npm install electron -g

Le package `electron est une version d’Electron prête à l’emploi. 

Une fois que c’est terminé, vous pouvez vérifier la version

electron -v

**Créer le manifeste**   

npm init

Vous devez répondre aux questions pour obtenir :

```json
{
  "name": "Covoiturage",
  "version": "1.0.0",
  "description": "Une application pour la gestion des membres de la communauté de covoiturage du lycée Pierre Poivre",
  "main": "main.js",
  "scripts": {
    "test": "electron ."
  },
  "author": "Bat'",
  "license": "GPL-3.0"
}
```

Vous pouvez ajouter des dépendences ...

**Le script principal**

Le script principal est ici main.js

Nous importons le module `electron`

```js
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
```

Puis vous ajoutez le code pour initialiser une fenêtre.

```js
let mainWindow;

function createWindow () {

  mainWindow = new BrowserWindow({width: 1800, height: 1200});  // on définit une taille pour notre fenêtre

  mainWindow.loadURL(`file://${__dirname}/index.html`);         // Charger le chemin absolu

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}
```

Pour appeler la méthode lorsque l’application démarre. 

```js
app.on('ready', createWindow);
```

Et pour ovus permettre de quitter l’application.

```js
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
```

**L’interface : un peu de HTML**

Dans votre script principal, vous avez charger une page appelée dans le fichier `index.html` ou vous allez écrire votre HTML.

Une application Electron multi-plateforme ! Elle permet entre autre de :

    création et gestion des fenêtres
    gestion des menus et du clavier
    utilisation de dialogues
    utilisation des fichiers
    communication entre les processus
    utilisation du presse-papier
    …


Pour vous faire une idée de quoi est capable Electron il existe une application de présentation sur le site : 

        https://github.com/electron/electron-api-demos
        
