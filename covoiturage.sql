-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 05 Mai 2019 à 14:22
-- Version du serveur :  5.7.26-0ubuntu0.16.04.1
-- Version de PHP :  5.6.40-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `covoiturage`
--

-- --------------------------------------------------------

--
-- Structure de la table `Adherant`
--

CREATE TABLE `Adherant` (
  `id_adh` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `pseudo` varchar(50) NOT NULL,
  `mdp` text NOT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Adherant`
--

INSERT INTO `Adherant` (`id_adh`, `nom`, `prenom`, `pseudo`, `mdp`, `status`) VALUES
(1, 'Goubeaux', 'Pierre', 'pierreG974', '$2y$10$65bUt8xUnQEpTf0N1bz6Iuy92Ldkbu.DF.CwdRfLdjPKQfXOTilwu', 0),
(2, 'Emma', 'Tony', 'EmmaT974', '$2y$10$ErxiCzY8jrO.l0urJsAYK.qgtj.ArlgygzPwVS101zpreaWuvWFD.', 0);

--
-- Déclencheurs `Adherant`
--
DELIMITER $$
CREATE TRIGGER `status_inscription` BEFORE INSERT ON `Adherant` FOR EACH ROW BEGIN
        IF NEW.status IS NULL 
        THEN SET NEW.status = "0";
        END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `est_passage`
--

CREATE TABLE `est_passage` (
  `id_trajet_est_passage` int(11) NOT NULL,
  `id_trajet` int(11) NOT NULL,
  `id_adh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `histo_trajet`
--

CREATE TABLE `histo_trajet` (
  `id_trajet` int(11) NOT NULL,
  `debut` int(11) NOT NULL,
  `fin` int(11) NOT NULL,
  `nb_places` int(11) NOT NULL,
  `date` date NOT NULL,
  `heur` time NOT NULL,
  `id_adh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Propose`
--

CREATE TABLE `Propose` (
  `id_trajet_Propose` int(11) NOT NULL,
  `id_trajet` int(11) NOT NULL,
  `id_adh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Trajet`
--

CREATE TABLE `Trajet` (
  `id_trajet` int(11) NOT NULL,
  `debut` int(11) NOT NULL,
  `fin` int(11) NOT NULL,
  `nb_places` int(11) NOT NULL,
  `date` date NOT NULL,
  `heur` time NOT NULL,
  `id_adh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `Trajet`
--
DELIMITER $$
CREATE TRIGGER `before_delete_trajet` BEFORE DELETE ON `Trajet` FOR EACH ROW BEGIN 
INSERT INTO histo_trajet (id_trajet,debut,fin,nb_places,date,heur,id_adh) VALUES (OLD.id_trajet,OLD.debut,OLD.fin,OLD.nb_places,OLD.date,OLD.heur,OLD.id_adh);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `Ville`
--

CREATE TABLE `Ville` (
  `id_ville` int(11) NOT NULL,
  `nom_ville` varchar(50) NOT NULL,
  `longitude` float DEFAULT NULL,
  `latitude` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Ville`
--

INSERT INTO `Ville` (`id_ville`, `nom_ville`, `longitude`, `latitude`) VALUES
(1, 'Les Avirons', 55.3334, -21.2419),
(2, 'Bras-Pranon', 55.6761, -20.9953),
(3, 'Cilaos', 55.4725, -21.1361),
(4, 'Entre-Deux', 55.4704, -21.2491),
(5, 'Etang-Sale', 55.367, -21.266),
(6, 'Petit-ile', 55.562, -21.3557),
(7, 'La Plaine des Palmistes', 55.7012, -21.1647),
(8, 'Le Port', 55.2871, -20.9393),
(9, 'La Posession', 55.336, -20.9265),
(10, 'Saint-Andre', 55.6504, -20.9605),
(11, 'Saint-Benoit', 55.7132, -21.0332),
(12, 'Saint-Denis', 55.4482, -20.8789),
(13, 'Sainte-Marie', 55.5492, -20.897),
(14, 'Sainte-Rose', 55.7922, -21.1273),
(15, 'Sainte-Suzanne', 55.6061, -20.9043),
(16, 'Saint-Joseph', 55.6192, -21.3786),
(17, 'Saint-Leu', 55.2868, -21.1663),
(18, 'Saint-Louis', 55.4091, -21.2867),
(19, 'Saint-Paul', 55.2697, -21.0097),
(20, 'Saint-Philippe', 55.7677, -21.3594),
(21, 'Saint-Pierre', 55.4778, -21.342),
(22, 'Salazie', 55.5391, -21.0278),
(23, 'Le Tampon', 55.5151, -21.2782),
(24, 'Les Trois-Bassins', 55.2949, -21.1058);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Adherant`
--
ALTER TABLE `Adherant`
  ADD PRIMARY KEY (`id_adh`);

--
-- Index pour la table `est_passage`
--
ALTER TABLE `est_passage`
  ADD PRIMARY KEY (`id_trajet_est_passage`) USING BTREE,
  ADD KEY `id_trajet` (`id_trajet`),
  ADD KEY `id_adh` (`id_adh`);

--
-- Index pour la table `Propose`
--
ALTER TABLE `Propose`
  ADD PRIMARY KEY (`id_trajet_Propose`),
  ADD KEY `id_trajet` (`id_trajet`),
  ADD KEY `id_adh` (`id_adh`);

--
-- Index pour la table `Trajet`
--
ALTER TABLE `Trajet`
  ADD PRIMARY KEY (`id_trajet`),
  ADD KEY `debut` (`debut`),
  ADD KEY `fin` (`fin`),
  ADD KEY `trajet_ibfk_3` (`id_adh`);

--
-- Index pour la table `Ville`
--
ALTER TABLE `Ville`
  ADD PRIMARY KEY (`id_ville`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Adherant`
--
ALTER TABLE `Adherant`
  MODIFY `id_adh` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `est_passage`
--
ALTER TABLE `est_passage`
  MODIFY `id_trajet_est_passage` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Propose`
--
ALTER TABLE `Propose`
  MODIFY `id_trajet_Propose` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Trajet`
--
ALTER TABLE `Trajet`
  MODIFY `id_trajet` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `est_passage`
--
ALTER TABLE `est_passage`
  ADD CONSTRAINT `est_passage_ibfk_1` FOREIGN KEY (`id_trajet`) REFERENCES `Trajet` (`id_trajet`),
  ADD CONSTRAINT `est_passage_ibfk_2` FOREIGN KEY (`id_adh`) REFERENCES `Adherant` (`id_adh`);

--
-- Contraintes pour la table `Propose`
--
ALTER TABLE `Propose`
  ADD CONSTRAINT `propose_ibfk_1` FOREIGN KEY (`id_trajet`) REFERENCES `Trajet` (`id_trajet`),
  ADD CONSTRAINT `propose_ibfk_2` FOREIGN KEY (`id_adh`) REFERENCES `Adherant` (`id_adh`);

--
-- Contraintes pour la table `Trajet`
--
ALTER TABLE `Trajet`
  ADD CONSTRAINT `trajet_ibfk_1` FOREIGN KEY (`debut`) REFERENCES `Ville` (`id_ville`),
  ADD CONSTRAINT `trajet_ibfk_2` FOREIGN KEY (`fin`) REFERENCES `Ville` (`id_ville`),
  ADD CONSTRAINT `trajet_ibfk_3` FOREIGN KEY (`id_adh`) REFERENCES `Adherant` (`id_adh`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
